const express = require('express');
const router = express.Router();
const userController = require('./../controllers/userControllers');
const Auth = require('./../auth')
// const Auth = require('./../auth')
// const User = require('./../models/User');

router.post('/login',(req,res)=>{
	userController.login(req.body).then((result) => { res.send(result)})
})

router.get('/',(req,res)=>{
	//console.log(`Welcome to users endpoint`)
	userController.getAllUser().then((result) =>{
		res.send(result)
	})
})

// router.post('/',(req,res)=>{
// 	//console.log(`Welcome to users endpoint`)
// 	//res.send(`Hello ${req.body}`)
// 	console.log(req.body);

// })

router.post('/email/exist',(req,res)=>{
	userController.checkEmail(req.body.email).then(result => {
		if(result){
			res.send(`The email is available to registration`)
		}else{
			res.send(`The email has been used in this app.`)
		}
	})
})

router.post('/register',(req,res)=>{
	userController.register(req.body).then((result)=>{
		res.send(result)
	})
})

router.get('/details',Auth.verify,(req,res)=>{
	let userData = Auth.decode(req.headers.authorization);
	userController.getUserProfile(userData).then((result)=>{
	res.send(result)
	})

})

module.exports = router;