const User = require('./../models/User');
const Auth = require('./../auth')
const bcrypt = require('bcrypt');

module.exports.login = (reqBody) => {
	const {email, password} = reqBody;
	return User.findOne({email: email}).then((result,error) =>{
		
		if(result==null){
			console.log('email null')
			return false
		}else{
			if(bcrypt.compareSync(password, result.password)){
			//if(result.password === password){
				//create a web token
				return {access: Auth.createAccessToken(result)}
		}else{
			return false;
		}

		}

	})
}

module.exports.getAllUser = () =>{
	return User.find({}).then((result)=>{
		return result
	})
}

module.exports.checkEmail = (email) =>{
	return User.findOne({email: email}).then((result,error)=>{
		if(result !== null){
		return false
		}else{
			if(result === null){
				return true
			}else{
				return error
			}
		}
	})
}

module.exports.register = (reqBody) =>{
	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email:email,
		password:bcrypt.hashSync(password,10),
		mobileNo:mobileNo,
		age:age
	})

	return newUser.save().then((result,error)=>{
		//console.log(result)
		if(result){
			return true
		}else{
			return error
		}
	})
}

module.exports.getUserProfile = (reqBody)=>{
	return User.findById({_id: reqBody.id}).then(result => {
		return result;
	})
}

module.exports.verify = (req, res, next) => {

	//where to get the token?
	let token = req.headers.authorization

	// jwt.verify(token, secret, function)

	if(typeof token !== "undefined"){

		let slicedToken = token.slice(7, token.length);

		return jwt.verify(slicedToken, secret, (err, data) => {
			if(err){
				res.send( {auth: "failed"} )
			} else {
				next()
			}
		})

	} else {
		res.send(false)
	}
}
