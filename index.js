const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes.js')

const app = express();
const port = 3001;

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(cors());

app.use('/api/users',userRoutes)


mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.9s26b.mongodb.net/courseBooking?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));



	



app.listen(port,()=>console.log(`Server running at port: ${port}`));