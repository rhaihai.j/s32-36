//JWT

// sign, verify, decode

//jwt.sign(payload, secret, {});



const jwt = require('jsonwebtoken');

const secret = "dshovchcgsayc";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

module.exports.decode = (token) => {
	//console.log(token)
	let slicedToken = token.slice(7,token.length)
	//console.log(jwt.decode(slicedToken,{complete:true}))
	return jwt.decode(slicedToken)
}

module.exports.verify = (req, res, next) => {

	//where to get the token?
	let token = req.headers.authorization

	// jwt.verify(token, secret, function)

	if(typeof token !== "undefined"){

		let slicedToken = token.slice(7, token.length);

		return jwt.verify(slicedToken, secret, (err, data) => {
			if(err){
				res.send( {auth: "failed"} )
			} else {
				next()
			}
		})

	} else {
		res.send(false)
	}
}